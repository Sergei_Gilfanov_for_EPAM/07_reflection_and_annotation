package com.epam.javatraining2016.refandannot;

// Did not work
//@Secured(integerArg =1)
public class Entity {
 
  public void publicA() {}
  private void privateA() {}
  protected void protectedA() {}
  
  @Secured(integerArg =1)
  public void publicB() { }

  @Secured(integerArg =1)
  private void privateB() { }

  @Secured(integerArg =1)
  protected void protectedB() {}

  @Secured(integerArg =1, stringArg="value")
  public void publicC() { }

  @Secured(integerArg =1, stringArg="value")
  private void privateC() { }

  @Secured(integerArg =1, stringArg="value")
  protected void protectedC() {}
  
  // Did not work
  //@Secured
  //public void publicD() { }

  // Did not work
  // @Secured(stringArg="value")
  //public void publicD() { }
  
  @Unsafe(value = 0)
  public void publicE() {}

}
