package com.epam.javatraining2016.refandannot;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Secured {
  int integerArg();
  String stringArg() default "strict";
}
