package com.epam.javatraining2016.refandannot;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class EntityUsage {

  public static void main(String[] args)
      throws InstantiationException, IllegalAccessException, ClassNotFoundException {
    Class<?> entityClass = Class.forName("com.epam.javatraining2016.refandannot.Entity");
    Method[] entityMethods = entityClass.getDeclaredMethods();
    for (Method method : entityMethods) {
      Secured annotation = method.getAnnotation(Secured.class);
      if (annotation != null && annotation.stringArg().equals("strict")) {
        System.out.println(method);
        System.out.println(annotation);
      }
    }

    System.out.println("=======");

    Object entity = entityClass.newInstance();
    entityMethods = entity.getClass().getDeclaredMethods();
    for (Method method : entityMethods) {
      System.out.println(method.getName());
      Annotation[] annotations = method.getAnnotations();
      for (Annotation annotation : annotations) {
        Class<?> annotationType = annotation.annotationType();
        System.out.println("    " + annotationType.getName());
        Method[] annotationMethods = annotationType.getDeclaredMethods();
        for (Method annotationMethod : annotationMethods) {
          System.out.print("        " + annotationMethod.getName());
          System.out.println(" " + annotationMethod.getReturnType());
        }
      }
    }
  }
}

